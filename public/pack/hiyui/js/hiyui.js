(function ($) {
    window.hiyui = function () {
        var dialogHtml = '<div class="modal fade" id="[Id]" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">' +
            '    <div class="modal-dialog modal-dialog-centered [Size]" role="document">' +
            '        <div class="modal-content">' +
            '            <div class="modal-header">' +
            '                <h5 class="modal-title" id="modalLabel">[Title]</h5>' +
            '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
            '                    <span aria-hidden="true">&times;</span>' +
            '                </button>' +
            '            </div>' +
            '            <div class="modal-body">' +
            '            </div>' +
            '            <div class="modal-footer">' +
            '                <button type="button" class="btn btn-success [BtnSub]">[BtnOk]</button>' +
            '            </div>' +
            '        </div>' +
            '    </div>' +
            '</div>';
        var loadHtml = '<div class="hiy-load-box"> <div class="hiy-loader-[Id]"></div></div>';
        var msgHtml = '<div aria-live="polite" aria-atomic="true" class="hiy-msgbox d-flex justify-content-center align-items-center">' +
            '  <div class="[Id]" role="alert" aria-live="assertive" aria-atomic="true">' +
            '    <div class="toast-body">[Content]</div>' +
            '  </div>' +
            '</div>';
        var reg = new RegExp("\\[([^\\[\\]]*?)\\]", 'igm');
        var that = this;
        that.msg = function (options, e) {
            options = $.extend({}, {
                id: options.id,
                content: options.content,
            }, options || {});
            let content = msgHtml.replace(reg, function (node, key) {
                return {
                    Id: options.id,
                    Content: options.content,
                }[key];
            });
            $('body').append(content);
            let expire = 2500;
            if (typeof options.expire != 'undefined') {
                expire = options.expire;
            }
            $('.'+options.id).toast({animation: true, autohide: false});
            setTimeout(function () {
                $('.'+options.id).toast('hide');
                $('.'+options.id).remove();
            }, expire);
            return this;
        }
        that.loading = function (options) {
            options = $.extend({}, {
                'id': typeof options.id != "undefined" ? options.id : 'a',
                parentId: options.parent,
            }, options || {});
            options.location = typeof options.location != "undefined" ? options.location : 'append';
            var content = loadHtml.replace(reg, function (node, key) {
                return {
                    Id: options.id,
                }[key];
            });
            this.onShow = function () {
                if(options.location == 'append'){
                    $(options.parentId).append(content);
                }else if(options.location == 'after'){
                    $(options.parentId).after(content);
                }
                return this;
            }
            this.onHide = function () {
                let target = $('.hiy-loader-'+options.id);
                target.parent('.hiy-load-box').hide();
                target.parent('.hiy-load-box').remove();
                return this;
            }
            return this;
        }
        that.dialog = function (options, callback) {
            options = $.extend({}, {
                id: '',
                title: '',
                uri: '',
                subBtn: typeof options.subBtn != "undefined" ? options.subBtn : '',
                size: typeof options.size != "undefined" ? options.size : '',
                onReady: function () {},
                onShown: function (e) {}
            }, options || {});

            var content = dialogHtml.replace(reg, function (node, key) {
                return {
                    Id: options.id,
                    Title: options.title,
                    BtnSub: options.subBtn,
                }[key];
            });
            $('body').append(content);
            var target = $('#' + options.id);
            if(typeof options.page != "undefined" && options.page == true){
                var loader = new that.loading({parent:'.modal-header', 'location':'after'});
                loader.onShow();
                setTimeout(function () {
                    loader.onHide();
                    target.find('.modal-body').load(options.uri);
                },400)
            }else{
                if(typeof options.content != "undefined"){
                    target.find('.modal-body').html(options.content);
                }
            }
            if (options.onReady()){
                options.onReady.call(target);
            }
            target.modal({
                backdrop:false,
                keyboard: true
            });
            if (typeof options.width != "undefined") {
                target.find('.modal-dialog').css({'width': options.width});
            }
            if (typeof options.size != "undefined") {
                target.find('.modal-dialog').addClass(options.size);
            }
            if (typeof options.btnok != "undefined") {
                target.find('.'+options.subBtn).text(options.btnok);
            }else{
                target.find('.'+options.subBtn).text("保存");
            }
            if (typeof options.btncl != "undefined") {
                target.find('.'+options.subBtn).after("<button type=\"button\" class=\"btn btn-secondary\">"+options.btncl+"</button>");
            }else{
                target.find('.btn-secondary').text('取消');
            }
            target.on('click', '.'+options.subBtn, function (e) {
                callback(1);
            });
            target.on('click', '.btn-secondary', function (e) {
                callback(0);
            });
            $("#"+options.id).on('hidden.bs.modal', function (e) {
                $(this).remove();
            })
        }
        return this;
    }();
})(jQuery);
