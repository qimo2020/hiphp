layui.use(['jquery', 'laydate', 'form'], function() {
    var $ = layui.jquery, form = layui.form, laydate = layui.laydate;
    $('input[lay-filter],select[lay-filter]').each(function(){
        var tagName = $(this)[0].tagName.toLowerCase(), filterType = '', inputType = '', fromField = $(this).attr('name'), filterName = $(this).attr('lay-filter'), fieldtips = $(this).attr('hi-filter-tips'), fieldTitle = $(this).attr('hi-filter-title'), field = $(this).attr('hi-filter-field'), action = $(this).attr('hi-filter-action');
        if(tagName == 'select'){
            filterType = 'select';
        }else{
            inputType = $(this).attr('type');
            if(inputType == 'checkbox' && $(this).attr('lay-skin') == 'switch'){
                filterType = 'switch';
            }else{
                switch (inputType) {
                    case 'checkbox':
                        filterType = 'checkbox';
                        break;
                    case 'radio':
                        filterType = 'radio';
                        break;
                }
            }
        }
        form.on(filterType+'('+filterName+')', function(data){
            $.ajax({
                url: action,
                data: {id: data.value},
                type: "post",
                dataType: "json",
                success: function (data) {
                    html(field, fieldTitle, data.data, fromField,fieldtips);
                    setTimeout(function () {
                        form.render();
                    }, 50)
                },
                error: function (data) {
                    $.messager.alert('错误', data.msg);
                }
            })
        })
    });

    function html(field, fieldTitle, data, fromField,fieldtips) {
        let html = '';
        switch (data.type) {
            case 'inputMulti':
                html = '<fieldset class="layui-elem-field check-multi"><div class="layui-field-box field-'+field+'" type="inputMulti">';
                let parent = $('.input-multi-inline'), multis = [];
                $.each(data.data, function(i, val){
                    html += '<div class="layui-inline"><label class="layui-form-label">'+val.name+'</label><div class="layui-input-inline">';
                    if(val.options == ''){
                        html += '<input type="text" class="layui-input field-text-'+field+'" name="'+field+'['+val.name+']" placeholder="" autocomplete="off">';
                    }else{
                        for(let j=0; j<val.options.length; j++){
                            if(val.type == 'radio'){
                                html += '<input type="radio" class="layui-input field-radio-'+field+'" name="'+field+'['+val.name+']" value="'+val.options[j]+'" title="'+val.options[j]+'">';
                            }else if(val.type == 'checkbox'){
                                $.each(val.options, function(x, v) {
                                    if(multis.indexOf(v) < 0){
                                        multis.push(v);
                                        html += '<input type="checkbox" class="field-checkbox-' + field + '" name="' + field + '[' + val.name + '][]" value="' + v + '" title="' + v + '" lay-skin="primary">';
                                    }
                                })
                            }
                        }
                    }
                    html += '</div><div class="layui-form-mid layui-word-aux">';
                    html += '</div></div>';
                });
                html += '</div></fieldset>';
                parent.html(html);
                break;
            case 'input':
                let preNode = $(".field-"+fromField).parent().parent();
                let currNode = $(".field-"+field).parent().parent();
                if(data.data.length > 0){
                    currNode.remove();
                    html = '<div class="layui-form-item"><label class="layui-form-label">'+fieldTitle+'</label><div class="layui-input-inline">';
                    html += '<input type="text" class="layui-input field-'+field+'" name="'+field+'" placeholder="" autocomplete="off">';
                    html += '</div><div class="layui-form-mid layui-word-aux">';
                    html += fieldtips;
                    html += '</div></div>';
                    preNode.after(html);
                }else{
                    currNode.remove();
                    html = '<div class="layui-form-item" style="display: none"><div class="layui-input-inline">';
                    html += '<input type="hidden" class="layui-input field-'+field+'" name="'+field+'" value="" placeholder="" autocomplete="off">';
                    html += '</div></div>';
                    preNode.after(html);
                }
                break;
            case 'select':
                let spreNode = $(".field-"+fromField).parent().parent();
                let scurrNode = $(".field-"+field).parent().parent();
                if(data.data.length > 0){
                    scurrNode.remove();
                    html = '<div class="layui-form-item"><label class="layui-form-label">'+fieldTitle+'</label><div class="layui-input-inline">';
                    html += '<select type="select" class="field-'+field+'" name="'+field+'" placeholder="" autocomplete="off">';
                    $.each(data.data, function(i, val) {
                        html += '<option value="'+val.id+'">'+val.title+'</option>';
                    })
                    html += '</select></select></div><div class="layui-form-mid layui-word-aux">';
                    html += fieldtips;
                    html += '</div></div>';
                    spreNode.after(html);
                }else{
                    scurrNode.remove();
                    html = '<div class="layui-form-item" style="display: none"><div class="layui-input-inline">';
                    html += '<input type="hidden" class="layui-input field-'+field+'" name="'+field+'" value="0" placeholder="" autocomplete="off">';
                    html += '</div></div>';
                    spreNode.after(html);
                }
                break;
            case 'datetime':
                let dpreNode = $(".field-"+fromField).parent().parent();
                let dcurrNode = $(".field-"+field).parent().parent();
                if(data.data.length > 0){
                    dcurrNode.remove();
                    html = '<div class="layui-form-item"><label class="layui-form-label">'+fieldTitle+'</label><div class="layui-input-inline">';
                    html += '<input type="text" class="layui-date-time layui-input field-'+field+'" name="'+field+'" placeholder="" autocomplete="off">';
                    html += '</div><div class="layui-form-mid layui-word-aux">';
                    html += fieldtips;
                    html += '</div></div>';
                    dpreNode.after(html);
                }else{
                    dcurrNode.remove();
                    html = '<div class="layui-form-item" style="display: none"><div class="layui-input-inline">';
                    html += '<input type="hidden" class="layui-input field-'+field+'" name="'+field+'" value="" placeholder="" autocomplete="off">';
                    html += '</div></div>';
                    dpreNode.after(html);
                }
                $('.layui-date-time').each(function(i) {
                    laydate.render({
                        elem: this
                        ,type: 'datetime'
                        ,trigger: 'click'
                    });
                });
                break;
        }
        return html;
    }
})