<?php declare(strict_types=1);
// +----------------------------------------------------------------------
// | HiPHP框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) http://www.hiphp.net
// +----------------------------------------------------------------------
// | HiPHP承诺基础框架永久免费开源，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 祈陌 <3411869134@qq.com>，开发者QQ群：829699898
// +----------------------------------------------------------------------
namespace app\system\admin;
use app\system\model\SystemLang as LangModel;
use think\facade\Db;
use plugins\builder\builder;
class Lang extends Base
{
    protected function initialize()
    {
        parent::initialize();
        checkPluginDepends(['builder']);
        $this->builder = new builder($this);
    }

    public function index($group = '', $type = '')
    {
        $status = $this->request->param('status/d', 0);
        $this->tabData['current'] = url('',['type'=>$type, 'group'=>$group, 'status'=>$status]);
        $this->tabData['tab'] = [
            [
                'title' => '语言包',
                'url' => url('', ['type'=>$type, 'group'=>$group, 'status' => 0]),
            ],
            [
                'title' => '语言变量',
                'url' => url('', ['type'=>$type, 'group'=>$group, 'status' => 1]),
            ]
        ];
        if (0 == $status) {
            if ($this->request->isPost()) {
                try{
                    $name = $this->request->param('name/s', 0);
                    $res = LangModel::setDefaultLanguage($group, $name);
                    if(false === $res){
                        return $this->response(0,'设置失败');
                    }
                    return $this->response(1,'设置成功');
                }catch (\Exception $e){
                    return $this->response(0,$e->getMessage());
                }
            }
            $formData = Db::name('system_language')->where(['group'=>$group, 'default'=>1])->find();
            $buildData = $this->buildFormLangSetting($group);
        }else if(1 == $status){
            if ($this->request->isPost()) {
                $get = $this->request->get();
                $post = $this->request->post();
                $langs = LangModel::where(['group'=>$get['group'], 'pack'=>$post['packid']])->select()->toArray();
                $insertArr = [];
                $langsArr = array_column($langs, 'name');
                foreach ($post as $k=>$v){
                    if(!empty($langsArr)){
                        if(in_array($k, $langsArr)) {
                            $res = LangModel::where(['group'=>$get['group'], 'pack'=>$post['packid'], 'name'=>$k])->update(['langvar'=>$v]);
                        }else {
                            if(!in_array($k, ['packid'])) {
                                $inserts['group'] = $get['group'];
                                $inserts['pack'] = $post['packid'];
                                $inserts['name'] = $k;
                                $inserts['langvar'] = $v;
                                $insertArr[] = $inserts;
                            }
                        }
                    }else{
                        if(!in_array($k, ['packid'])){
                            $inserts['group'] = $get['group'];
                            $inserts['pack'] = $post['packid'];
                            $inserts['name'] = $k;
                            $inserts['langvar'] = $v;
                            $insertArr[] = $inserts;
                        }
                    }
                }
                if(!empty($insertArr)){
                    $insertArr = array_unique($insertArr, SORT_REGULAR);
                    $res = LangModel::insertAll($insertArr);
                }
                cache('lang_default_'.$group, null);
                return $this->response(1,'已修改');
            }
            $default = Db::name('system_language')->where(['group'=>$group, 'default'=>1])->find();
            $formData = LangModel::where(['group'=>$group, 'pack'=>$default['id']])->find();
            $buildData = $this->buildFormLang($group, $type, $default['name']);
        }
        $this->assign('formData', $formData);
        $this->assign('tabData', $this->tabData);
        $this->assign('buildData', $buildData);
        $this->assign('tabType', 3);
        return $this->view('build/form');
    }

    private function buildFormLang($group, $type, $packName){
        $result = $this->builder->buildData();
        $result['buildForm']['action'] = $this->tabData['current'];
        $langs = LangModel::getDefaultLang($group);
        $fieldItems = [];
        $langInfos = 0 == $type ? include_once base_path() . $group . '/lang/' . $packName . '.php' : include_once root_path() . 'plugins/' . $group . '/lang/' . $packName . '.php';
        $i = 0;
        foreach ($langInfos as $key=>$v){
            $fieldItems[$i] = ['title'=>$key, 'type'=>'line'];
            $i++;
            foreach ($v as $kk=>$vv){
                if(!empty($langs)){
                    foreach ($langs as $kkk=>$vvv){
                        $keys = array_keys($vv);
                        $fieldItems[$i]['title'] = $kk;
                        if($vvv['name'] === $keys[0]){
                            $fieldItems[$i]['name'] = $vvv['name'];
                            $fieldItems[$i]['type'] = iconv_strlen($vvv['langvar'],"UTF-8") > 100 ? 'textarea' : 'text';
                            $fieldItems[$i]['value'] = $vvv['langvar'];
                            $fieldItems[$i]['tips'] = '调用字段为 [ '.$vvv['name'].' ]';
                        }else{
                            $fieldItems[$i]['name'] = $keys[0];
                            $fieldItems[$i]['type'] = iconv_strlen($vv[$keys[0]],"UTF-8") > 100 ? 'textarea' : 'text';
                            $fieldItems[$i]['value'] = $vv[$keys[0]];
                            $fieldItems[$i]['tips'] = '调用字段为 [ '.$keys[0].' ]';
                        }
                        $i++;
                    }
                }else{
                    $keys = array_keys($vv);
                    $fieldItems[$i]['title'] = $kk;
                    $fieldItems[$i]['name'] = $keys[0];
                    $fieldItems[$i]['type'] = iconv_strlen($vv[$keys[0]],"UTF-8") > 100 ? 'textarea' : 'text';
                    $fieldItems[$i]['value'] = $vv[$keys[0]];
                    $fieldItems[$i]['tips'] = '调用字段为 [ '.$keys[0].' ]';
                    $i++;
                }

            }
        }
        $languege = Db::name('system_language')->where(['name'=>$packName])->find();
        $fieldItems[] = ['type'=>'hidden','name'=>'packid', 'value'=>$languege['id']];

        $fieldItems = array_values(array_unique($fieldItems, SORT_REGULAR));

        $result['buildForm']['items'] = array_column($fieldItems, null, 'name');

        return $result;
    }

    private function buildFormLangSetting($group){
        $list = Db::name('system_language')->where(['group'=>$group])->select()->toArray();
        $listArr = array_combine(array_column($list, 'id'), array_column($list, 'name'));
        $result = $this->builder->buildData();
        $result['buildForm']['action'] = $this->tabData['current'];
        $result['buildForm']['items'] = [
            [
                'name'=>'name',
                'type'=>'select',
                'title'=>'语言包选择',
                'value'=>'',
                'tips'=>'',
                'options'=>$listArr
            ]
        ];
        return $result;
    }

}
